package com.anuj.data.di

import com.anuj.data.network.ProductsService
import com.anuj.data.repository.ProductsRepoImpl
import com.anuj.domain.repository.ProductsRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun providesProductsRepo(service: ProductsService): ProductsRepo {
        return ProductsRepoImpl(service)
    }
}