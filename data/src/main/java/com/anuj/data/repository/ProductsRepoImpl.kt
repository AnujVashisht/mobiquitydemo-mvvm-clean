package com.anuj.data.repository

import com.anuj.data.network.ProductsService
import com.anuj.data.network.dto.toDomainModel
import com.anuj.domain.entites.IProductDetails
import com.anuj.domain.repository.ProductsRepo
import io.reactivex.Single
import javax.inject.Singleton

@Singleton
class ProductsRepoImpl(private val service: ProductsService) :
    ProductsRepo {
    override fun getProducts(): Single<List<IProductDetails>> {
        return service.getProducts().map { it.toDomainModel() }
    }
}