package com.anuj.data.network.dto

import com.google.gson.annotations.SerializedName

data class PriceResponseModel(
    @SerializedName("amount") val amount: String,
    @SerializedName("currency") val currency: String
)