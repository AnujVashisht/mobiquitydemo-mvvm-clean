package com.anuj.data.network

import com.anuj.data.network.dto.CategoryResponseModel
import io.reactivex.Single
import javax.inject.Singleton

@Singleton
class ProductsService(private val api: ProductsApi) {
    fun getProducts(): Single<List<CategoryResponseModel>> = api.getCategories()
}