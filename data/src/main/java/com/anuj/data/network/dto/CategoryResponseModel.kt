package com.anuj.data.network.dto

import com.anuj.data.di.NetworkModule
import com.anuj.domain.entites.Category
import com.anuj.domain.entites.IProductDetails
import com.anuj.domain.entites.Price
import com.anuj.domain.entites.Product
import com.google.gson.annotations.SerializedName
import java.util.*

data class CategoryResponseModel(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
    @SerializedName("products") val products: List<ProductResponseModel>
)

fun List<CategoryResponseModel>.toDomainModel(): List<IProductDetails> {
    val list: MutableList<IProductDetails> = mutableListOf()
    this.forEach { model ->
        val products = model.products.map {
            Product(
                it.id,
                it.categoryId,
                it.name,
                NetworkModule.BASE_URL.substring(0, NetworkModule.BASE_URL.length - 1).plus(it.url),
                it.description,
                Price(
                    it.salePrice.amount,
                    getCurrencyCode(it.salePrice.currency)
                )
            )
        }
        list.add(Category(model.id, model.name, model.description, products))
        products.forEach {
            list.add(it)
        }
    }
    return list
}

private fun getCurrencyCode(code: String): String {
    return Currency.getInstance(code).symbol
}