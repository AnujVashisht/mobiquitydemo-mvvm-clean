package com.anuj.data.network

import com.anuj.data.network.dto.CategoryResponseModel
import io.reactivex.Single
import retrofit2.http.GET

interface ProductsApi {
    @GET("/") fun getCategories(): Single<List<CategoryResponseModel>>
}