package com.anuj.data.network

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class ProductsServiceTest {

    private val api: ProductsApi = mock()
    private lateinit var service: ProductsService

    @Before
    fun setup() {
        service = ProductsService(api)
    }

    @Test
    fun `when get products is called, should call api`() {
        service.getProducts()

        verify(api).getCategories()
    }
}