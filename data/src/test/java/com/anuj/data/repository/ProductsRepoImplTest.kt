package com.anuj.data.repository

import com.anuj.data.network.ProductsService
import com.anuj.data.network.dto.CategoryResponseModel
import com.anuj.domain.entites.Category
import com.anuj.domain.repository.ProductsRepo
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class ProductsRepoImplTest {

    private val service: ProductsService = mock()
    private val category: CategoryResponseModel = mock()
    private lateinit var repository: ProductsRepoImpl

    @Before
    fun setUp() {
        repository = ProductsRepoImpl(service)
        whenever(category.id).thenReturn("id")
        whenever(category.description).thenReturn("description")
        whenever(category.name).thenReturn("name")
        whenever(category.products).thenReturn(emptyList())
    }

    @Test
    fun getProducts() {
        whenever(service.getProducts()).thenReturn(Single.just(listOf(category)))

        repository.getProducts()

        verify(service).getProducts()
    }
}