# Mobiquity Demo Application
A small application which fetch data from an Api and display products and click on product displays the details page.

## Architecture
- Kotlin
- Clean Architecture(Data, Domain and UI modules)
- MVVM
- Dagger2 dependency injection
- Retrofit
- RX Java2
- Navigation Component
- Live Data

### Tasks completed
- Completed the UI tasks
- Unit tests are covered