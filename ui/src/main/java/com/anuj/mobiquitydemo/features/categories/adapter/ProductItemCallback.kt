package com.anuj.mobiquitydemo.features.categories.adapter

import com.anuj.domain.entites.Product

interface ProductItemCallback {
    fun onProductItemClicked(product: Product)
}