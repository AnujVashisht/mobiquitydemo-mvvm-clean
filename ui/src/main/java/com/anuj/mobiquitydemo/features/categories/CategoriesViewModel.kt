package com.anuj.mobiquitydemo.features.categories

import androidx.lifecycle.MutableLiveData
import com.anuj.domain.entites.Category
import com.anuj.domain.entites.IProductDetails
import com.anuj.domain.usecase.ProductsUseCase
import com.anuj.mobiquitydemo.base.framework.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CategoriesViewModel @Inject constructor(private val productsUseCase: ProductsUseCase) :
    BaseViewModel() {

    private var categories: List<IProductDetails>? = null
    val state: MutableLiveData<CategoriesViewState> = MutableLiveData()

    fun getCategories() {
        state.value = CategoriesViewState.Loading

        if (categories == null) {
            addDisposable(
                productsUseCase.getCategories()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { categories ->
                            this.categories = categories
                            state.value = CategoriesViewState.DataReady(categories)
                        },
                        { error ->
                            state.value = CategoriesViewState.Error(error.message.toString())
                        })
            )
        } else {
            state.value = CategoriesViewState.DataReady(categories!!)
        }
    }
}