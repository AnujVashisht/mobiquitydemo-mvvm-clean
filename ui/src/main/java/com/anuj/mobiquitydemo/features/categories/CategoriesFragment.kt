package com.anuj.mobiquitydemo.features.categories

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.mobiquitydemo.R
import com.anuj.domain.entites.Product
import com.anuj.mobiquitydemo.base.framework.BaseFragment
import com.anuj.mobiquitydemo.features.categories.adapter.CategoriesAdapter
import com.anuj.mobiquitydemo.features.categories.adapter.ProductItemCallback
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_categories.*

class CategoriesFragment : BaseFragment<CategoriesViewModel>(), ProductItemCallback {
    private val adapter = CategoriesAdapter(this)

    override fun getViewModelClass(): Class<CategoriesViewModel> = CategoriesViewModel::class.java

    override fun layoutId() = R.layout.fragment_categories

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter
        viewModel.state.observe(viewLifecycleOwner, Observer {
            when (it) {
                is CategoriesViewState.Loading -> showProgress()
                is CategoriesViewState.Error -> {
                    hideProgress()
                    Snackbar.make(view, it.message, Snackbar.LENGTH_LONG).setAction(getString(R.string.action_try_again)) {
                        viewModel.getCategories()
                    }.show()
                }
                is CategoriesViewState.DataReady -> {
                    hideProgress()
                    displayData(it)
                }
            }
        })
        viewModel.getCategories()
    }

    override fun onProductItemClicked(product: Product) {
        val action =
            CategoriesFragmentDirections.actionCategoriesFragmentToProductsFragment(
                productImg = product.url,
                productName = product.name,
                productPrice = "${product.salePrice.currency} ${product.salePrice.amount}",
                productDesc = if (product.description.isEmpty()) {
                    getString(R.string.no_description)
                } else {
                    product.description
                }
            )
        Navigation.findNavController(view!!).navigate(action)
    }

    private fun displayData(it: CategoriesViewState.DataReady) {
        adapter.setData(it.info)
    }
}