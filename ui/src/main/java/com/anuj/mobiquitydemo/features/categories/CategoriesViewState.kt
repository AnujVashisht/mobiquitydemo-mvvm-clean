package com.anuj.mobiquitydemo.features.categories

import com.anuj.domain.entites.IProductDetails

sealed class CategoriesViewState {
    object Loading : CategoriesViewState()
    data class Error(val message: String) : CategoriesViewState()
    data class DataReady(val info: List<IProductDetails>) : CategoriesViewState()
}