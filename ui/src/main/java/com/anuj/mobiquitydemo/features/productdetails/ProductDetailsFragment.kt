package com.anuj.mobiquitydemo.features.productdetails

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.android.mobiquitydemo.R
import com.anuj.mobiquitydemo.base.framework.BaseFragment
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_products.*

class ProductDetailsFragment : BaseFragment<ProductsViewModel>() {
    private val args: ProductDetailsFragmentArgs by navArgs()

    override fun getViewModelClass(): Class<ProductsViewModel> = ProductsViewModel::class.java

    override fun layoutId() = R.layout.fragment_products

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(requireContext())
            .load(args.productImg)
            .centerCrop()
            .into(img)
        title.text = args.productName
        subtitle.text = if (args.productDesc.isEmpty()) {
            getString(R.string.no_description)
        } else {
            args.productDesc
        }
        price.text = args.productPrice
    }
}