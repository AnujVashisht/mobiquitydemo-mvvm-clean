package com.anuj.mobiquitydemo.features.categories.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.mobiquitydemo.R
import com.anuj.domain.entites.Category
import com.anuj.domain.entites.IProductDetails
import com.anuj.domain.entites.Product
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_category_layout.view.title
import kotlinx.android.synthetic.main.item_product_layout.view.*

class CategoriesAdapter constructor(private val callback: ProductItemCallback) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private enum class ItemType {
        CATEGORY,
        ITEM
    }

    private var data: List<IProductDetails> = emptyList()

    fun setData(data: List<IProductDetails>) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ItemType.CATEGORY.ordinal -> {
                CategoryViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_category_layout, parent, false
                    )
                )
            }
            ItemType.ITEM.ordinal -> {
                ProductViewHolder(
                    LayoutInflater
                        .from(parent.context)
                        .inflate(R.layout.item_product_layout, parent, false)
                )
            }
            else -> throw IllegalStateException("No matching type holder found!")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CategoryViewHolder -> {
                val category = data[position] as Category
                holder.title.text = category.name
            }
            is ProductViewHolder -> {
                val product = data[position] as Product
                Glide.with(holder.itemView.context)
                    .load(product.url)
                    .centerCrop()
                    .into(holder.img)
                holder.title.text = product.name
                holder.subtitle.text = if (product.description.isEmpty()) {
                    holder.itemView.context.getString(R.string.no_description)
                } else {
                    product.description
                }
                holder.price.text = "${product.salePrice.currency} ${product.salePrice.amount}"
                holder.itemView.setOnClickListener { callback.onProductItemClicked(product) }
            }
        }
    }

    override fun getItemCount() = data.size

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is Category -> ItemType.CATEGORY.ordinal
            is Product -> ItemType.ITEM.ordinal
            else -> -1
        }
    }

    class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.title
    }

    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.title
        val subtitle: TextView = itemView.subtitle
        val price: TextView = itemView.price
        val img: ImageView = itemView.img
    }
}