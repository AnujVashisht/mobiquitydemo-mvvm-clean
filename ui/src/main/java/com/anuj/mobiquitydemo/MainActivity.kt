package com.anuj.mobiquitydemo

import android.os.Bundle
import android.view.View
import com.android.mobiquitydemo.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : DaggerAppCompatActivity(), ProgressViewCallback {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun showLoader() {
        progressView.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        progressView.visibility = View.GONE
    }
}