package com.anuj.mobiquitydemo

interface ProgressViewCallback {
    fun showLoader()
    fun hideLoader()
}