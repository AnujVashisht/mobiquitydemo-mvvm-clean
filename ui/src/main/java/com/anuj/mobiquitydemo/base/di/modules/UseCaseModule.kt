package com.anuj.mobiquitydemo.base.di.modules

import com.anuj.domain.repository.ProductsRepo
import com.anuj.domain.usecase.ProductsUseCase
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {
    @Provides
    fun providesProductsUseCase(repo: ProductsRepo) = ProductsUseCase(repo)
}