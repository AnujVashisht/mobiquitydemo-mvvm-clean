package com.anuj.mobiquitydemo.base.di.modules

import android.app.Application
import android.content.Context
import com.anuj.data.di.NetworkModule
import com.anuj.data.di.RepositoryModule
import com.anuj.mobiquitydemo.base.di.viewmodels.ViewModelModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [
        (NetworkModule::class),
        (RepositoryModule::class),
        (ViewModelModule::class),
        (UseCaseModule::class)
    ]
)
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }
}