package com.anuj.mobiquitydemo.base.di.modules

import com.anuj.mobiquitydemo.MainActivity
import com.anuj.mobiquitydemo.base.di.scopes.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeMainActivityInjector(): MainActivity
}