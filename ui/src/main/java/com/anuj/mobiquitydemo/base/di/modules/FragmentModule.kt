package com.anuj.mobiquitydemo.base.di.modules

import com.anuj.mobiquitydemo.base.di.scopes.CategoriesScope
import com.anuj.mobiquitydemo.features.categories.CategoriesFragment
import com.anuj.mobiquitydemo.features.productdetails.ProductDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class FragmentModule {
    @ContributesAndroidInjector
    @CategoriesScope
    abstract fun contributeCategoriesFragment(): CategoriesFragment

    @ContributesAndroidInjector
    @CategoriesScope
    abstract fun contributeProductsFragment(): ProductDetailsFragment
}