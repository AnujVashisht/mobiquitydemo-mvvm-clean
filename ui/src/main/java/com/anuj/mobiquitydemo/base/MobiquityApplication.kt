package com.anuj.mobiquitydemo.base

import com.anuj.data.di.NetworkModule
import com.anuj.mobiquitydemo.base.di.AppComponent
import com.anuj.mobiquitydemo.base.di.DaggerAppComponent
import com.anuj.mobiquitydemo.base.framework.BaseApplication

open class MobiquityApplication : BaseApplication() {
    override val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .application(this)
            .network(NetworkModule())
            .build()
    }
}