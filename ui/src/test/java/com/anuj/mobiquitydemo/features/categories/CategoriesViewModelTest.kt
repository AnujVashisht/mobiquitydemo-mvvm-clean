package com.anuj.mobiquitydemo.features.categories

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.anuj.domain.entites.Category
import com.anuj.domain.usecase.ProductsUseCase
import com.anuj.mobiquitydemo.features.BaseUnitTest
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class CategoriesViewModelTest: BaseUnitTest() {

    private val productsUseCase: ProductsUseCase = mock()
    private lateinit var observer: Observer<CategoriesViewState>
    private val actualStateValues = mutableListOf<CategoriesViewState>()
    private val viewModel = CategoriesViewModel(productsUseCase)

    @Rule
    @JvmField
    val instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @Before
    fun beforeEach() {
        observer = spy(Observer {
            it ?: return@Observer
            actualStateValues.plusAssign(it)
        })
    }

    @After
    fun afterEach() {
        actualStateValues.clear()
    }

    @Test
    fun `when get categories has no errors`() {
        val response : List<Category> = emptyList()
        whenever(productsUseCase.getCategories()).thenReturn(Single.just(response))

        viewModel.state.observeForever(observer)
        viewModel.getCategories()

        assertEquals(CategoriesViewState.Loading, actualStateValues[0])
        assertEquals(CategoriesViewState.DataReady(response), actualStateValues[1])
    }

    @Test
    fun `when categories are already loaded, should not load again`() {
        val response : List<Category> = emptyList()
        whenever(productsUseCase.getCategories()).thenReturn(Single.just(response))

        viewModel.state.observeForever(observer)
        viewModel.getCategories()
        viewModel.getCategories()

        verify(productsUseCase, times(1)).getCategories()
        assertEquals(CategoriesViewState.Loading, actualStateValues[0])
        assertEquals(CategoriesViewState.DataReady(response), actualStateValues[1])
    }

    @Test
    fun `when get categories has errors`() {
        whenever(productsUseCase.getCategories()).thenReturn(Single.error(Throwable("Some error")))

        viewModel.state.observeForever(observer)
        viewModel.getCategories()

        assertEquals(CategoriesViewState.Loading, actualStateValues[0])
        assertEquals(CategoriesViewState.Error("Some error"), actualStateValues[1])
    }
}