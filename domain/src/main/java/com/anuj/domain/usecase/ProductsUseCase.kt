package com.anuj.domain.usecase

import com.anuj.domain.repository.ProductsRepo

class ProductsUseCase(private val repo: ProductsRepo) {
    fun getCategories() = repo.getProducts()
}