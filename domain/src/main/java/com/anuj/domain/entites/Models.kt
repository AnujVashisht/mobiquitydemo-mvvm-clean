package com.anuj.domain.entites

interface IProductDetails

data class Category(
    val id: String,
    val name: String,
    val description: String,
    val products: List<Product>
) : IProductDetails

data class Product(
    val id: String,
    val categoryId: String,
    val name: String,
    val url: String,
    val description: String,
    val salePrice: Price
) : IProductDetails

data class Price(
    val amount: String,
    val currency: String
)