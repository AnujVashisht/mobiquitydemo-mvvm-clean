package com.anuj.domain.repository

import com.anuj.domain.entites.Category
import com.anuj.domain.entites.IProductDetails
import io.reactivex.Single

interface ProductsRepo {
    fun getProducts(): Single<List<IProductDetails>>
}