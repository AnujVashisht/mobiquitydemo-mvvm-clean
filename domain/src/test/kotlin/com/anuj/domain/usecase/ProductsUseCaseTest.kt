package com.anuj.domain.usecase

import com.anuj.domain.repository.ProductsRepo
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Test

class ProductsUseCaseTest {

    private val repo: ProductsRepo = mock()
    private lateinit var useCase: ProductsUseCase

    @Before
    fun setup() {
        useCase = ProductsUseCase(repo)
    }

    @Test
    fun `get categories should call get products from repository`() {
        useCase.getCategories()

        verify(repo).getProducts()
    }
}